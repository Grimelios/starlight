﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Entities;
using Starlight.Json;
using Starlight.UI.Menus;

namespace Starlight.Control
{
	public static class Mappings
	{
		private const string Folder = "Controls/";

		static Mappings()
		{
			Player = JsonUtilities.Deserialize<PlayerControls>(Folder + "DefaultPlayerControls.json", false);
			Menu = JsonUtilities.Deserialize<MenuControls>(Folder + "DefaultMenuControls.json", false);
		}

		public static PlayerControls Player { get; }
		public static MenuControls Menu { get; }
	}
}
