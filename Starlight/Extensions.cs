﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace Starlight
{
	public static class Extensions
	{
		public static Vector2 ToIntegers(this Vector2 value)
		{
			return new Vector2((int)value.X, (int)value.Y);
		}

		public static float Cross(this Vector2 v1, Vector2 v2)
		{
			return v1.X * v2.Y - v1.Y * v2.X;
		}

		public static void SendMessage(this NetPeer peer, NetOutgoingMessage message, NetDeliveryMethod method)
		{
			foreach (NetConnection connection in peer.Connections)
			{
				peer.SendMessage(message, connection, method);
			}
		}
	}
}
