﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Starlight.Interfaces;
using Starlight.Networking.Encoders;

namespace Starlight.Networking
{
	public class GamePeer
	{
		private NetPeer peer;
		private NetworkEncoder[] encoders;

		private MessageTypes[] messageTypeArray;

		public GamePeer()
		{
			NetPeerConfiguration configuration = new NetPeerConfiguration("Starlight")
			{
				Port = 14242
			};

			peer = new NetPeer(configuration);
			encoders = new NetworkEncoder[]
			{
				new PlayerJoinEncoder()
			};

			messageTypeArray = new []
			{
				MessageTypes.PlayerJoin,
				MessageTypes.PlayerLeave,
				MessageTypes.PlayerKick,
				MessageTypes.PlayerNameChange,
				MessageTypes.PlayerReady,
				MessageTypes.PlayerInvite,
				MessageTypes.Chat,
				MessageTypes.LobbyNameChange
			};

			Messaging.Subscribe(MessageTypes.Network, (data, dt) => SendData((NetworkData)data));
		}

		private void SendData(NetworkData data)
		{
			NetOutgoingMessage message = peer.CreateMessage();
			encoders[(int)data.Type].Encode(data.Data, message);
			peer.SendMessage(message, data.Method);
		}

		public void Connect(string host, int port)
		{
		}

		public void Update()
		{
			NetIncomingMessage message;

			while ((message = peer.ReadMessage()) != null)
			{
				switch (message.MessageType)
				{
					case NetIncomingMessageType.Data:
						int index = message.ReadByte();

						Messaging.Send(messageTypeArray[index], encoders[index].Decode(message));

						break;
				}

				peer.Recycle(message);
			}
		}
	}
}
