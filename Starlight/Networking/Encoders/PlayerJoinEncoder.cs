﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Starlight.Networking.Encoders
{
	public class PlayerJoinEncoder : NetworkEncoder
	{
		public override void Encode(object data, NetOutgoingMessage message)
		{
			message.Write((string)data);
		}

		public override object Decode(NetIncomingMessage message)
		{
			return null;
		}
	}
}
