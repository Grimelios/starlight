﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Starlight.Networking.Encoders
{
	public abstract class NetworkEncoder
	{
		public abstract void Encode(object data, NetOutgoingMessage message);
		public abstract object Decode(NetIncomingMessage message);
	}
}
