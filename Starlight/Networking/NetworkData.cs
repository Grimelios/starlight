﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Starlight.Networking
{
	public enum NetworkDataTypes
	{
		PlayerJoin,
		PlayerLeave,
		PlayerKick,
		PlayerNameChange,
		PlayerReady,
		PlayerInvite,
		Chat,
		LobbyNameChange
	}

	public class NetworkData
	{
		public NetworkData(NetworkDataTypes type, object data, NetDeliveryMethod method)
		{
			Type = type;
			Data = data;
			Method = method;
		}

		public NetworkDataTypes Type { get; }
		public NetDeliveryMethod Method { get; }

		public object Data { get; }
	}
}
