﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Networking.Data
{
	public class PlayerJoinData
	{
		public PlayerJoinData(string name)
		{
			Name = name;
		}

		public string Name { get; }
	}
}
