﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Interfaces
{
	public interface IRotatable
	{
		float Rotation { get; set; }
	}
}
