﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Entities;

namespace Starlight.Interfaces
{
	public interface IInteractive
	{
		void OnInteract(Player player);
	}
}
