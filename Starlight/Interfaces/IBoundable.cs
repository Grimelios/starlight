﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Core;

namespace Starlight.Interfaces
{
	public interface IBoundable : IPositionable
	{
		Bounds Bounds { get; }
	}
}
