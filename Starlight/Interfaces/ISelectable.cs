﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Interfaces
{
	public interface ISelectable : IBoundable
	{
		bool Enabled { get; set; }

		void OnHover();
		void OnUnhover();
		void OnHighlight();
		void OnUnhighlight();
		bool OnSelect();
	}
}
