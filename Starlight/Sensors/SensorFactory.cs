﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Entities;
using Starlight.Entities.Core;
using Starlight.Shapes;

namespace Starlight.Sensors
{
	public static class SensorFactory
	{
		private static SensorManager manager;

		public static void Initialize(SensorManager m)
		{
			manager = m;
		}

		public static Sensor CreateSensor(SensorTypes type, AbstractShape shape, object userData)
		{
			Sensor sensor = new Sensor(type)
			{
				Shape = shape,
				UserData = userData,
				Manager = manager
			};

			manager.SensorList.Add(sensor);

			return sensor;
		}
	}
}
