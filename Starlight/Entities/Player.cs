﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Entities.Core;

namespace Starlight.Entities
{
	public class Player : LivingEntity
	{
		public Player() : base(EntityTypes.Player)
		{
		}
	}
}
