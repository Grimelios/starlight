﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Shapes.Data
{
	public class BoxData : ShapeData
	{
		public BoxData(float width, float height) : base(ShapeTypes.Box)
		{
			Width = width;
			Height = height;
		}

		public float Width { get; }
		public float Height { get; }
	}
}
