﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Structures
{
	public class SinglyLinkedListNode<T>
	{
		public SinglyLinkedListNode(T data)
		{
			Data = data;
		}

		public T Data { get; }

		public SinglyLinkedListNode<T> Next { get; set; }
	}
}
