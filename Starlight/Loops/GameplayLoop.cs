﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Starlight.Entities;
using Starlight.Entities.Core;
using Starlight.Physics;
using Starlight.Physics.Shapes;
using Starlight.Sensors;
using Starlight.UI;
using Starlight.UI.Hud;
using Starlight.UI.Menus;

namespace Starlight.Loops
{
	public class GameplayLoop : GameLoop
	{
		private Scene scene;
		private SensorManager sensorManager;
		private SensorVisualizer sensorVisualizer;
		private PhysicsAccumulator accumulator;
		private PhysicsVisualizer physicsVisualizer;
		private HeadsUpDisplay headsUpDisplay;
		private MenuManager menuManager;

		private bool paused;

		public GameplayLoop()
		{
			Messaging.Subscribe(MessageTypes.Pause, (data, dt) => paused = (bool)data);
		}

		public override void Initialize(Camera camera, MenuManager menuManager)
		{
			this.menuManager = menuManager;

			World world = new World(new Vector2(0, PhysicsConstants.Gravity));
			PhysicsFactory.Initialize(world);
			PhysicsUtilities.Initialize(world);

			sensorManager = new SensorManager();
			sensorVisualizer = new SensorVisualizer(sensorManager);
			accumulator = new PhysicsAccumulator(world);
			physicsVisualizer = new PhysicsVisualizer(world);
			headsUpDisplay = new HeadsUpDisplay();
			
			SensorFactory.Initialize(sensorManager);

			scene = new Scene();
		}

		public override void Update(float dt)
		{
			if (paused)
			{
				menuManager.Update(dt);

				return;
			}

			accumulator.Update(dt);
			scene.Update(dt);
			sensorManager.Update(dt);
			headsUpDisplay.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.World);
			scene.Draw(sb);

			if (physicsVisualizer.Enabled)
			{
				physicsVisualizer.Draw(sb);
			}

			if (sensorVisualizer.Enabled)
			{
				sensorVisualizer.Draw(sb);
			}

			sb.End();

			sb.Begin(Coordinates.Screen);
			headsUpDisplay.Draw(sb);
			sb.End();
		}
	}
}
