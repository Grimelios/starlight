﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Input.Controls;
using Starlight.Networking;
using Starlight.UI.Menus;
using Starlight.UI.Screens;

namespace Starlight.Loops
{
	public class TitleLoop : GameLoop
	{
		private MenuManager menuManager;

		public override void Initialize(Camera camera, MenuManager menuManager)
		{
			this.menuManager = menuManager;
		}

		public override void Update(float dt)
		{
			menuManager.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.Screen);
			menuManager.Draw(sb);
			sb.End();
		}
	}
}
