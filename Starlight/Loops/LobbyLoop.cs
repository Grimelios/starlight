﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Networking;
using Starlight.UI.Menus;
using Starlight.UI.Screens;

namespace Starlight.Loops
{
	public class LobbyLoop : GameLoop
	{
		private GamePeer peer;
		private ScreenManager screenManager;

		public override void Initialize(Camera camera, MenuManager menuManager)
		{
			peer = new GamePeer();
			screenManager = new ScreenManager();
		}

		public override void Update(float dt)
		{
			peer.Update();
			screenManager.Update(dt);
		}

		public override void Draw(SuperBatch sb)
		{
			sb.Begin(Coordinates.Screen);
			screenManager.Draw(sb);
			sb.End();
		}
	}
}
