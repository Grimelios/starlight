﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Interfaces;
using Starlight.UI.Menus;

namespace Starlight.Loops
{
	public abstract class GameLoop : IDynamic, IRenderable
	{
		public abstract void Initialize(Camera camera, MenuManager menuManager);
		public abstract void Update(float dt);
		public abstract void Draw(SuperBatch sb);
	}
}
