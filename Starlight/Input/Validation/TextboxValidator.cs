﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Input.Validation
{
	public abstract class TextboxValidator
	{
		public abstract bool Validate(string value);
	}
}
