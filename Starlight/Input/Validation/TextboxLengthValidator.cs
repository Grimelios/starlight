﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Starlight.Input.Validation
{
	public class TextboxLengthValidator : TextboxValidator
	{
		private int length;

		public TextboxLengthValidator(int length)
		{
			this.length = length;
		}

		public override bool Validate(string value)
		{
			return value.Length == length;
		}
	}
}
