﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Core;
using Starlight.Input.Data;
using Starlight.Interfaces;

namespace Starlight.Input.Controls
{
	public class TextboxTester : IRenderable
	{
		private Textbox textbox;
		private SpriteText spriteText;
		private SpriteText cursorText;
		private SpriteText insertText;

		public TextboxTester()
		{
			textbox = new Textbox("Test", 300, 30, () => { });
			spriteText = new SpriteText("Default", null)
			{
				Position = new Vector2(150)
			};

			cursorText = new SpriteText("Default", "0")
			{
				Position = new Vector2(150, 170)
			};

			insertText = new SpriteText("Default", "false")
			{
				Position = new Vector2(150, 190)
			};

			Messaging.Subscribe(MessageTypes.Keyboard, (data, dt) => textbox.HandleKeyboard((KeyboardData)data));
		}

		public void Draw(SuperBatch sb)
		{
			spriteText.Value = textbox.Value;
			spriteText.Draw(sb);

			cursorText.Value = textbox.Cursor.ToString();
			cursorText.Draw(sb);

			insertText.Value = textbox.Insert ? "true" : "false";
			insertText.Draw(sb);
		}
	}
}
