﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using Starlight.Input.Controls.Rendering;
using Starlight.Input.Data;

namespace Starlight.Input.Controls
{
	public class Textbox : InputControl
	{
		private static char[] numericSpecials =
		{
			')',
			'!',
			'@',
			'#',
			'$',
			'%',
			'^',
			'&',
			'*',
			'('
		};

		private StringBuilder builder;
		private Action submitAction;
		private TextboxRenderer renderer;

		public Textbox(string label, int width, int height, Action submitAction) : base(width, height)
		{
			this.submitAction = submitAction;

			Label = label;
			builder = new StringBuilder();
			renderer = new TextboxRenderer(this);
			Renderer = renderer;
			Focusable = true;
			Validators = new List<Func<string, bool>>();
		}

		public string Label { get; set; }
		public string Value
		{
			get => builder.ToString();
			set
			{
				builder.Clear();
				builder.Append(value);

				renderer.Value = value;
			}
		}

		public int Cursor { get; private set; }

		public bool Insert { get; private set; }

		public List<Func<string, bool>> Validators { get; }

		public bool Validate()
		{
			string value = Value;

			return Validators.Any(v => v(value));
		}

		public void HandleKeyboard(KeyboardData data)
		{
			if (data.Query(Keys.Enter, InputStates.PressedThisFrame))
			{
				submitAction();

				return;
			}

			bool shift = data.Query(InputStates.Held, Keys.LeftShift, Keys.RightShift);
			bool capsLock = InputUtilities.CheckLock(LockKeys.CapsLock);
			bool numLock = InputUtilities.CheckLock(LockKeys.NumLock);

			// Even if numlock is off, special numpad functions can be activated by holding shift.
			if (!numLock)
			{
				numLock = shift;
			}

			bool home = data.Query(Keys.Home, InputStates.PressedThisFrame);
			bool end = data.Query(Keys.End, InputStates.PressedThisFrame);
			bool left = data.Query(Keys.Left, InputStates.PressedThisFrame);
			bool right = data.Query(Keys.Right, InputStates.PressedThisFrame);
			bool backspace = data.Query(Keys.Back, InputStates.PressedThisFrame);
			bool delete = data.Query(Keys.Delete, InputStates.PressedThisFrame);

			if (!numLock)
			{
				home |= data.Query(Keys.NumPad7, InputStates.PressedThisFrame);
				end |= data.Query(Keys.NumPad1, InputStates.PressedThisFrame);
				left |= data.Query(Keys.NumPad4, InputStates.PressedThisFrame);
				right |= data.Query(Keys.NumPad6, InputStates.PressedThisFrame);
				delete |= data.Query(Keys.Decimal, InputStates.PressedThisFrame);
			}

			// If home and end are pressed on the same frame, home takes priority.
			if (home)
			{
				Cursor = 0;
			}
			else if (end)
			{
				Cursor = builder.Length;
			}

			if (left ^ right)
			{
				if (left)
				{
					Cursor = Cursor > 0 ? --Cursor : 0;
				}
				else
				{
					Cursor = Cursor < builder.Length ? ++Cursor : builder.Length;
				}
			}

			// Backspace and delete are intentionally done after moving the cursor and before adding new characters.
			if (backspace && builder.Length > 0 && Cursor > 0)
			{
				builder.Remove(Cursor - 1, 1);
				Cursor--;

				return;
			}

			if (delete && builder.Length > 0 && Cursor < builder.Length)
			{
				builder.Remove(Cursor, 1);
			}

			if (data.Query(Keys.Insert, InputStates.PressedThisFrame))
			{
				Insert = !Insert;
			}

			foreach (Keys key in data.KeysPressedThisFrame)
			{
				char? character = GetCharacter(key, shift, capsLock, numLock);

				if (character.HasValue)
				{
					if (Insert && Cursor < builder.Length)
					{
						builder[Cursor] = character.Value;
					}
					else
					{
						builder.Insert(Cursor, character.Value);
					}

					Cursor++;
				}
			}

			renderer.Value = Value;
		}

		private char? GetCharacter(Keys key, bool shift, bool capsLock, bool numLock)
		{
			string value = key.ToString();

			if (key >= Keys.A && key <= Keys.Z)
			{
				if (capsLock)
				{
					shift = !shift;
				}

				return shift ? value[0] : value.ToLower()[0];
			}

			if (key >= Keys.D0 && key <= Keys.D9)
			{
				return !shift ? value[1] : numericSpecials[value[1] - '0'];
			}

			// Special numpad functions are handled before reaching this point.
			if (numLock)
			{
				if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
				{
					return value[6];
				}

				if (key == Keys.Decimal)
				{
					return '.';
				}
			}

			switch (key)
			{
				case Keys.OemComma: return shift ? '<' : ',';
				case Keys.OemPeriod: return shift ? '>' : '.';
				case Keys.OemQuestion: return shift ? '?' : '/';
				case Keys.OemSemicolon: return shift ? ':' : ';';
				case Keys.OemQuotes: return shift ? '"' : '\'';
				case Keys.OemOpenBrackets: return shift ? '{' : '[';
				case Keys.OemCloseBrackets: return shift ? '}' : ']';
				case Keys.OemPipe: return shift ? '|' : '\\';
				case Keys.OemMinus: return shift ? '_' : '-';
				case Keys.OemPlus: return shift ? '+' : '=';
				case Keys.OemTilde: return shift ? '~' : '`';

				// These four keys work even if numlock is turned off.
				case Keys.Add: return '+';
				case Keys.Subtract: return '-';
				case Keys.Multiply: return '*';
				case Keys.Divide: return '/';

				case Keys.Space: return ' ';
			}

			return null;
		}
	}
}
