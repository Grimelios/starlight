﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Starlight.Core;
using Starlight.Interfaces;

namespace Starlight.Input.Controls.Rendering
{
	public class TextboxRenderer : InputRenderer
	{
		private Textbox parent;
		private SpriteText labelText;
		private SpriteText valueText;

		public TextboxRenderer(Textbox parent)
		{
			this.parent = parent;

			SpriteFont font = ContentLoader.LoadFont("Default");

			labelText = new SpriteText(font, parent.Label);
			valueText = new SpriteText(font, parent.Value);
		}

		public override Vector2 Position
		{
			get => Vector2.Zero;
			set
			{
				labelText.Position = value - new Vector2(0, 20);
				valueText.Position = value;
			}
		}

		public string Value
		{
			set => valueText.Value = value;
		}

		public override void Draw(SuperBatch sb)
		{
			labelText.Draw(sb);
			valueText.Draw(sb);

			Primitives.DrawBounds(sb, parent.Bounds, Color.White);
		}
	}
}
