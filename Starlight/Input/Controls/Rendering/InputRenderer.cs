﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Interfaces;

namespace Starlight.Input.Controls.Rendering
{
	public abstract class InputRenderer : IPositionable, IRenderable
	{
		private Vector2 position;

		public virtual Vector2 Position
		{
			get => position;
			set => position = value;
		}

		public abstract void Draw(SuperBatch sb);
	}
}
