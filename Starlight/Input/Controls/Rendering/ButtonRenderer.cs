﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Core;
using Starlight.Interfaces;

namespace Starlight.Input.Controls.Rendering
{
	public class ButtonRenderer : InputRenderer
	{
		private Button parent;
		private SpriteText spriteText;

		public ButtonRenderer(Button parent)
		{
			this.parent = parent;

			spriteText = new SpriteText("Default", parent.Text, Alignments.Center);
		}

		public override Vector2 Position
		{
			get => spriteText.Position;
			set => spriteText.Position = value;
		}

		public string Text
		{
			set => spriteText.Value = value;
		}

		public override void Draw(SuperBatch sb)
		{
			spriteText.Draw(sb);

			Primitives.DrawBounds(sb, parent.Bounds, Color.White);
		}
	}
}
