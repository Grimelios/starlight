﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Interfaces;
using Starlight.Json;

namespace Starlight.UI
{
	public abstract class UIContainer : IDynamic, IRenderable
	{
		protected UIContainer(string jsonFile)
		{
			Elements = JsonUtilities.Deserialize<UIElement[]>("UI/" + jsonFile, true);

			int width = Resolution.WindowWidth;
			int height = Resolution.WindowHeight;

			foreach (UIElement element in Elements)
			{
				Alignments alignment = element.Alignment;

				bool left = (alignment & Alignments.Left) == Alignments.Left;
				bool right = (alignment & Alignments.Right) == Alignments.Right;
				bool top = (alignment & Alignments.Top) == Alignments.Top;
				bool bottom = (alignment & Alignments.Bottom) == Alignments.Bottom;

				int offsetX = element.OffsetX;
				int offsetY = element.OffsetY;
				int x = left ? offsetX : (right ? width - offsetX : width / 2);
				int y = top ? offsetY : (bottom ? height - offsetY : height / 2);

				element.Position = new Vector2(x, y);
				element.Initialize();
			}
		}

		protected UIElement[] Elements { get; }

		public T GetElement<T>() where T : UIElement
		{
			return (T)Elements.FirstOrDefault(t => t is T);
		}

		public void Update(float dt)
		{
			foreach (UIElement element in Elements)
			{
				if (element.Visible)
				{
					element.Update(dt);
				}
			}
		}

		public void Draw(SuperBatch sb)
		{
			foreach (UIElement element in Elements)
			{
				if (element.Visible)
				{
					element.Draw(sb);
				}
			}
		}
	}
}
