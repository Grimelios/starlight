﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Starlight.Core;

namespace Starlight.UI
{
	public abstract class UIElement : Container2D
	{
		[JsonProperty]
		public Alignments Alignment { get; set; }

		[JsonProperty]
		public int OffsetX { get; set; }

		[JsonProperty]
		public int OffsetY { get; set; }

		public virtual void Initialize()
		{
		}
	}
}
