﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Input.Data;
using Starlight.Loops;

namespace Starlight.UI.Menus
{
	public class PauseMenu : Menu
	{
		public PauseMenu()
		{
			string[] items =
			{
				"Resume",
				"Settings",
				"Exit to Title",
				"Exit to Desktop"
			};

			Visible = false;
		}

		public override void Submit(int index)
		{
		}
	}
}
