﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.UI.Menus.Items;

namespace Starlight.UI.Menus
{
	public class TitleMenu : Menu
	{
		public TitleMenu()
		{
			string[] names =
			{
				"Create Lobby",
				"Restore Lobby",
				"Join Lobby,",
				"Settings",
				"Exit"
			};

			List<MenuItem> items = new List<MenuItem>();

			for (int i = 0; i < names.Length; i++)
			{
				items.Add(new BasicMenuItem(names[i], i));
			}

			SelectedIndex = 1;
			items[SelectedIndex].OnHover();
			items[0].Enabled = false;

			Initialize(items);
		}

		public override void Submit(int index)
		{
			switch (index)
			{
				// Create lobby
				case 0: break;

				// Restore lobby
				case 1: break;

				// Settings
				case 2: break;

				// Exit
				case 3: Messaging.Send(MessageTypes.Exit, null); break;
			}
		}
	}
}
