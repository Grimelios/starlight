﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Input.Data;
using Starlight.Interfaces;

namespace Starlight.UI.Screens
{
	public class ScreenManager : IDynamic, IRenderable
	{
		private Screen[] screens;
		private Screen activeScreen;

		public ScreenManager()
		{
			screens = new Screen[]
			{
				new LobbyScreen()
			};

			activeScreen = screens[0];

			Messaging.Subscribe(MessageTypes.Mouse, (data, dt) => HandleMouse((MouseData)data));
			Messaging.Subscribe(MessageTypes.Input, (data, dt) => HandleInput((AggregateData)data));
		}

		private void HandleMouse(MouseData data)
		{
			activeScreen?.HandleMouse(data);
		}

		private void HandleInput(AggregateData data)
		{
			activeScreen?.HandleInput(data);
		}

		public void Update(float dt)
		{
			activeScreen?.Update(dt);
		}

		public void Draw(SuperBatch sb)
		{
			activeScreen?.Draw(sb);
		}
	}
}
