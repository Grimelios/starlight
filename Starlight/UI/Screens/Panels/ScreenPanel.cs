﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Input.Controls;
using Starlight.Interfaces;

namespace Starlight.UI.Screens.Panels
{
	public abstract class ScreenPanel : UIElement
	{
		public Screen Parent { get; set; }

		public virtual InputControl[] GetControls()
		{
			return null;
		}
	}
}
