﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Networking.Data;

namespace Starlight.UI.Screens.Panels
{
	public class PlayerListPanel : ScreenPanel
	{
		private const int LineSpacing = 40;

		private List<PlayerLine> lines;

		public PlayerListPanel()
		{
			lines = new List<PlayerLine>();
			lines.Add(new PlayerLine("Grimelios"));

			Messaging.Subscribe(MessageTypes.PlayerJoin, (data, dt) => Add((PlayerJoinData)data));
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				Functions.PositionItems(lines, value, new Vector2(0, LineSpacing));

				base.Position = value;
			}
		}

		private void Add(PlayerJoinData data)
		{
			lines.Add(new PlayerLine(data.Name));
		}

		public override void Update(float dt)
		{
			lines.ForEach(l => l.Update(dt));
		}

		public override void Draw(SuperBatch sb)
		{
			lines.ForEach(l => l.Draw(sb));
		}
	}
}
