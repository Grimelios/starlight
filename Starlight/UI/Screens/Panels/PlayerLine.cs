﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Core;

namespace Starlight.UI.Screens.Panels
{
	public class PlayerLine : Container2D
	{
		private SpriteText nameText;

		public PlayerLine(string playerName)
		{
			nameText = new SpriteText("Default", playerName, Alignments.Left);
			Centered = true;
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				nameText.Position = value;

				base.Position = value;
			}
		}

		public override void Draw(SuperBatch sb)
		{
			nameText.Draw(sb);
		}
	}
}
