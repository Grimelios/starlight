﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Starlight.Input.Controls;

namespace Starlight.UI.Screens.Panels
{
	public class ConnectionPanel : ScreenPanel
	{
		private Textbox ipTextbox;
		private Textbox portTextbox;
		private Button connectButton;

		public ConnectionPanel()
		{
			ipTextbox = new Textbox("IP Address", 100, 30, Connect);
			portTextbox = new Textbox("Port", 100, 30, Connect);
			connectButton = new Button("Connect", 80, 40, Connect);
		}

		public override Vector2 Position
		{
			get => base.Position;
			set
			{
				ipTextbox.Position = value;
				portTextbox.Position = new Vector2(ipTextbox.Bounds.Right, value.Y);
				connectButton.Position = new Vector2(portTextbox.Bounds.Right, value.Y);

				base.Position = value;
			}
		}

		private void Connect()
		{
		}

		public override InputControl[] GetControls()
		{
			return new InputControl[]
			{
				ipTextbox,
				portTextbox,
				connectButton
			};
		}

		public override void Draw(SuperBatch sb)
		{
			ipTextbox.Draw(sb);
			portTextbox.Draw(sb);
			connectButton.Draw(sb);
		}
	}
}
