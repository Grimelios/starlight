﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starlight.Core;
using Starlight.Input.Controls;
using Starlight.Input.Data;
using Starlight.Input.Sets;
using Starlight.Json;
using Starlight.UI.Screens.Panels;

namespace Starlight.UI.Screens
{
	public abstract class Screen : UIContainer
	{
		protected Screen(string jsonFile) : base(jsonFile)
		{
			ControlSet = new SelectableSet<InputControl>();

			foreach (ScreenPanel panel in Elements.Cast<ScreenPanel>())
			{
				panel.Parent = this;

				InputControl[] controls = panel.GetControls();

				if (controls != null)
				{
					ControlSet.Items.AddRange(controls);
				}
			}
		}

		public SelectableSet<InputControl> ControlSet { get; }

		public void HandleMouse(MouseData data)
		{
			ControlSet.HandleMouse(data);
		}

		public void HandleInput(AggregateData data)
		{
			if (ControlSet.FocusedItem != null)
			{
				Textbox textbox = ControlSet.FocusedItem as Textbox;
				textbox?.HandleKeyboard((KeyboardData)data[InputTypes.Keyboard]);
			}
		}
	}
}
