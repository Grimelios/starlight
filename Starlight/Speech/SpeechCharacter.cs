﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Starlight.Core;
using Starlight.Interfaces;

namespace Starlight.Speech
{
	public class SpeechCharacter : IPositionable, IDynamic, IRenderable
	{
		private SpriteText spriteText;

		public SpeechCharacter(SpriteFont font, char value)
		{
			spriteText = new SpriteText(font, value.ToString());
		}

		public Vector2 Position
		{
			get => spriteText.Position;
			set => spriteText.Position = value;
		}

		public void Update(float dt)
		{
		}

		public void Draw(SuperBatch sb)
		{
			spriteText.Draw(sb);
		}
	}
}
